import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { DetailsComponent } from './details/details.component';

const routes: Routes = [
  {
    path: '',
      component: AppComponent,
      children: [
          { path: '', component: HomeComponent, pathMatch: 'full'},
          { path: 'search/:id', component: SearchComponent, pathMatch: 'full' },
          { path: 'details/:id', component: DetailsComponent, pathMatch: 'full' },
      ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
