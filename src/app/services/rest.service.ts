import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(
    private http: HttpClient
  ) { }

  get(url: string, args = {}) {
		return this.http.get(url, args).toPromise()
			.then(response => response)
	}

  searchCity(name) {
    return this.get('https://api.openweathermap.org/data/2.5/find', {params: {"q":name,"APPID": '76d1b43ba3695cfae59aa9f7dc9b4877', 'units':'metric'}});
  }
  searchForecast(id) {
    return this.get('https://api.openweathermap.org/data/2.5/forecast', {params: {"id":id,"APPID": '76d1b43ba3695cfae59aa9f7dc9b4877', 'units':'metric'}});
  }
  searchWeather(id) {
    return this.get('https://api.openweathermap.org/data/2.5/weather', {params: {"id":id,"APPID": '76d1b43ba3695cfae59aa9f7dc9b4877', 'units':'metric'}});
  }
}
