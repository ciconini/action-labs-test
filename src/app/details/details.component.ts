import { Component, OnInit } from '@angular/core';
import { RestService } from '../services/rest.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  constructor(
    public rest: RestService,
    private router: Router,
    private _route: ActivatedRoute,
  ) { }

  id: any;
  idCity: any;
  weather: any = [];
  forecasts: any = [];
  data : any = [];
  city : any = [];
  date: any;
  sunset: any;
  sunrise: any;
  loaded : boolean = false;
  
  
  ngOnInit() {
    
    this.load();

  }

  search() {
    let params  = this._route.snapshot.paramMap;
    let id = params.get("id");
    this.router.navigateByUrl(this.router.url.replace(id, this.city.name));
  }

  load() {
    let params  = this._route.snapshot.paramMap;
    this.idCity  = params.get("id").toString();

    this.date = new Date();
    
    this.rest.searchWeather(this.idCity).then((data:any) => {
      this.weather = data;
      this.sunset = new Date(this.weather.sys.sunset*1000);
      let sunsetTime = this.sunset.getHours() + ':' + this.sunset.getMinutes();
      this.sunset = sunsetTime;
      this.sunrise = new Date(this.weather.sys.sunrise*1000);
      let sunriseTime = this.sunrise.getHours() + ':' + this.sunrise.getMinutes();
      this.sunrise = sunriseTime;
      this.loaded = true;
    })
    this.rest.searchForecast(this.idCity).then((data:any) => {
      this.data = data;
      this.forecasts = data.list;
      data.list.forEach(element => {
        let time = new Date(element.dt*1000);
        element.dt = time;
      });
    })
  }

}
