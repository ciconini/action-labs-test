import { Component, OnInit } from '@angular/core';
import { RestService } from '../services/rest.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor(
    public rest:RestService,
    private router: Router,
    private _route: ActivatedRoute,
  ) { }
  
  id: any;
  cities: any = [];

  city: {
    name: string
  } = {
    name: ''
  };
  
  ngOnInit() {
    this.load();
    this._route.params.subscribe(params => {
        this.load();
    });
  }
  search() {
    let params  = this._route.snapshot.paramMap;
    let id = params.get("id");
    this.router.navigateByUrl(this.router.url.replace(id, this.city.name));
  }

  load() {
    let params  = this._route.snapshot.paramMap;
    this.id  = params.get("id").toString();
    this.city.name = this.id;

    
    this.rest.searchCity(this.id).then((data:any) => {
      this.cities = data.list;
      console.log(this.cities);
    })
  }

}
