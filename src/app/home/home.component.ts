import { Component, OnInit } from '@angular/core';
import { RestService } from '../services/rest.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    public rest:RestService,
    private router: Router,
  ) { }

  city: {
    name: string
  } = {
    name: ''
  };

  ngOnInit() {
  }
  search() {
    this.router.navigate(['/search/'+this.city.name]);
  }

}
